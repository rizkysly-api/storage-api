<?php

// List all applications keys with the following information:
// - App secrets to check the request signatures
// - The folder where files should be stored (within the path listed below)
// - The file types that are allowed to be stored by the app
define('APP_KEYS', [
    'APP_KEY' => [
        'secret' => 'APP_SECRET',
        'storage' => 'folder_in_storage_path',
        'files' => [
            'extention' => 'mime type'
        ]
    ]
]);

// This app interacts with the notification api, so we need to define the application key and secret
define('APP_KEY', '');
define('APP_SECRET', '');

// Where do we store files? Preferably on a location not directly accessable by clients
define('STORAGE_PATH', '/storage_path');
define('PERMANENTLY_DELETE_AFTER', 60 * 60 * 24 * 365);

// What is the URL of the notification api?
define('NOTIFICATION_API', 'https://api.domain.com');

// Talking to a database require database credentials
define('DB_HOST', 'localhost');
define('DB_DATABASE', '');
define('DB_USERNAME', '');
define('DB_PASSWORD', '');

// Some throtteling stuff
define('FAILED_ATTEMPTS_NUMBER', 10); // How many failed attempts can an ip make
define('FAILED_ATTEMPTS_MINUTES', 10); // In how much time
define('TIMESTAMP_AGE_SECONDS', 300); // What can the timestamp difference be