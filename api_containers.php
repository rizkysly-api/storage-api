<?php

// To allow multiple datasets, we work with containers

class api_containers
{

    // This is what all functions want
    public static $path = '';

    // The container must exist on the filesystem and be an uuidv4
    public static function is(): void
    {
        // Do we have a container? Is it an uuidv4?
        if (!isset($_POST['container']) || !api_uuidv4::check($_POST['container'])) {
            api_security::generateError('404 File not found', false);
        }

        // Create the path for the files
        $folder = APP_KEYS[$_SERVER['HTTP_X_APPLICATION_KEY']]['storage'];
        $container = $_POST['container'];
        for ($i = 1; $i < 16; $i += 2) {
            $container = substr_replace($container, '/', $i, 0);
        }
        self::$path = STORAGE_PATH . '/' . $folder . '/' . $container;

        if (!is_dir(self::$path) && !in_array(pathinfo($_SERVER['REQUEST_URI'], PATHINFO_BASENAME), ['addcontainer', 'delnotifications'])) {
            api_security::generateError('410 Container is not online', false);
        }

        // It is possible the container was marked deleted, so check for that
        if (is_file(self::$path . '/deleted')) {

            // Remove the device/container pair from the database, they will be notified with the demise
            $stmt = api_database::prepare('DELETE FROM storage_device_containers WHERE device_uuid = ? AND container_uuid = ?');
            $stmt->bind_param('ss', $_POST['device'], $_POST['container']);
            $stmt->execute();
            api_database::check();
            $stmt->close();

            api_security::generateError('204 No content', false); // The client should do something with this
        }

        // Record the device/container pair with a date, so we know where to send push notifications and detect orphant containers in the future
        $stmt = api_database::prepare('INSERT INTO storage_device_containers (device_uuid, container_uuid) VALUE ( ? , ? ) ON DUPLICATE KEY UPDATE last = NOW()');
        $stmt->bind_param('ss', $_POST['device'], $_POST['container']);
        $stmt->execute();
        api_database::check();
        $stmt->close();
    }

    // Adding a container is the act of creating a folder on the file system
    public static function add(): void
    {
        // Make sure the container does not exist, or the client should create a new uuid
        if (is_dir(self::$path)) {
            return;
        }

        // This should work, or the server has problems and the client should try again later
        if (!mkdir(self::$path, 0777, true)) {
            api_security::generateError('500 Internal Server Error');
        }
    }

    // Removing a container is the act of deleting the folder contents and place a deleted file for future reference
    public static function delete(): void
    {
        // https://stackoverflow.com/a/26423999
        array_map('unlink', glob(self::$path . '/*.*'));

        // Mark the container as deleted by placing a deleted file in it
        if (!touch(self::$path . '/deleted')) {
            api_security::generateError('500 Internal Server Error');
        }

        // Remove the device/container pair from the database
        $stmt = api_database::prepare('DELETE FROM storage_device_containers WHERE device_uuid = ? AND container_uuid = ?');
        $stmt->bind_param('ss', $_POST['device'], $_POST['container']);
        $stmt->execute();
        api_database::check();
        $stmt->close();
    }
}
