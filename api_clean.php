<?php

// What do we need to exchange files?

class api_clean
{
    // Return a list of files/md5 strings 
    public static function deletedfiles($dir = null): void
    {
        $dir = ($dir) ? $dir : STORAGE_PATH;

        $files = scandir($dir);

        if (count($files) == 2) {
            rmdir($dir);
            return;
        }

        foreach ($files as $file) {
            if (in_array($file, ['.', '..'])) {
                continue;
            }

            if (is_dir($dir . '/' . $file)) {
                self::deletedfiles($dir . '/' . $file);
                continue;
            }

            if ($file == 'deleted') {
                if (filemtime($dir . '/' . $file) < time() - PERMANENTLY_DELETE_AFTER) {
                    self::emptyfolder($dir);
                }
                break;
            }
        }
    }

    private static function emptyfolder($dir)
    {
        $files = scandir($dir);
        foreach ($files as $file) {
            if (in_array($file, ['.', '..'])) {
                continue;
            }

            unlink($dir . '/' . $file);
        }
    }
}
