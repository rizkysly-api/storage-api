-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Gegenereerd op: 18 nov 2018 om 20:07
-- Serverversie: 5.7.24-0ubuntu0.18.04.1
-- PHP-versie: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `api_rizkysly`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `storage_device_containers`
--

CREATE TABLE `storage_device_containers` (
  `device_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `container_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `last` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `storage_device_containers`
--
ALTER TABLE `storage_device_containers`
  ADD PRIMARY KEY (`device_uuid`,`container_uuid`),
  ADD KEY `app_uuid` (`container_uuid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
