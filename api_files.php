<?php

// What do we need to exchange files?

class api_files
{
    // Return a list of files/md5 strings 
    public static function list(): void
    {
        header('Content-Type: application/json');

        // See if the files have changed
        $dirtime = null;
        if (is_dir(api_containers::$path)) {
            $dirtime = filemtime(api_containers::$path . '/.');
        }

        $filetime = null;
        if (is_file(api_containers::$path . '/.md5.json')) {
            $filetime = filemtime(api_containers::$path . '/.md5.json');
        }

        // If different, create a new .md5 file
        if ($dirtime != $filetime) {
            $list = [];
            $dir = scandir(api_containers::$path);
            foreach ($dir as $file) {
                // Exclude folders and hidden files
                if (!is_file(api_containers::$path . '/' . $file) || strpos($file, '.') === 0) {
                    continue;
                }

                $list[] = [
                    'md5' => md5(file_get_contents(api_containers::$path . '/' . $file)),
                    'file' => $file
                ];
            }

            // Delete the old one (so we know the directory modified date really is updated)
            if (is_file(api_containers::$path . '/.md5.json')) {
                unlink(api_containers::$path . '/.md5.json');
            }

            file_put_contents(api_containers::$path . '/.md5.json', json_encode($list));

            // Print the list and terminate the script
            echo json_encode($list);

            return;
        }

        // Return the md5 file to buffer
        readfile(api_containers::$path . '/.md5.json');
    }

    // Get a file from storage
    public static function get(): void
    {
        // Do we have a filename?
        if (!isset($_POST['file'])) {
            api_security::generateError('400 Bad Request (file)', false);
        }

        // Make sure the requested file is not a hidden file or contain any slashes
        if (strpos($_POST['file'], '/') !== false || strpos($_POST['file'], '.') === 0) {
            api_security::generateError('400 Bad Request (file)', false);
        }

        // Check if file exists
        if (!is_file(api_containers::$path . '/' . $_POST['file'])) {
            api_security::generateError('404 File not found', false);
        }

        // Return the file to buffer
        header('Content-Type: ' . mime_content_type(api_containers::$path . '/' . $_POST['file']));
        readfile(api_containers::$path . '/' . $_POST['file']);
    }

    // Add a file to storage
    public static function add(): void
    {
        // Do we have a filename and file contents?
        if (!isset($_POST['file']) || !isset($_POST['data'])) {
            api_security::generateError('400 Bad Request (file or data)', false);
        }

        // Make sure the file is not a hidden file or contain any slashes
        if (strpos($_POST['file'], '/') !== false || strpos($_POST['file'], '.') === 0) {
            api_security::generateError('400 Bad Request (file)', false);
        }

        // Get the allowed extentions and mime types
        $data = base64_decode($_POST['data']);

        $filetypes = APP_KEYS[$_SERVER['HTTP_X_APPLICATION_KEY']]['files'];
        if (count($filetypes) > 0) {
            // Check if filename has the appropiate file extention
            $ext = pathinfo($_POST['file'], PATHINFO_EXTENSION);
            if (!isset($filetypes[$ext])) {
                api_security::generateError('400 Bad Request (exention)', false);
            }

            // Check if the filecontents is what we expect it to be
            $file_info = new finfo(FILEINFO_MIME_TYPE);
            if ($file_info->buffer($data) != $filetypes[$ext]) {
                api_security::generateError('400 Bad Request (data)' . $ext . ' - ' . $file_info->buffer($data), false);
            }
        }

        // Store the file to disk
        file_put_contents(api_containers::$path . '/' . $_POST['file'], $data);

        // Force folder date update
        touch(api_containers::$path . '/.update');
        unlink(api_containers::$path . '/.update');

        // Let all other devices know a file is added
        api_notifications::update($_POST['file'], 'add-file');
    }

    // Delete a file from storage
    public static function delete(): void
    {
        // Do we have a filename?
        if (!isset($_POST['file'])) {
            api_security::generateError('400 Bad Request (file)', false);
        }

        // Make sure the requested file is not a hidden file or contain any slashes
        if (strpos($_POST['file'], '/') !== false || strpos($_POST['file'], '.') === 0) {
            api_security::generateError('400 Bad Request (file)', false);
        }

        // Check if file exists
        if (!is_file(api_containers::$path . '/' . $_POST['file'])) {
            api_security::generateError('404 File not found', false);
        }

        // Delete the file
        if (!unlink(api_containers::$path . '/' . $_POST['file'])) {
            api_security::generateError('500 Internal Server Error');
        }

        // Let all other devices know a file is added
        api_notifications::update($_POST['file'], 'delete-file');
    }
}
