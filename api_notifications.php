<?php

// When you rely on local notifications, you have a problem when using multiple devices
// Actions changed on one device will not update on other devices. 
// The app needs opening for local notifications to be reset. They can't be set in the background.
// To solve this, all notifications should be sent to the api so it can sent it when the time comes to all linked devices.

class api_notifications
{

    // Set the new notifications (don't forget to delete the old once firts)
    public static function set(): void
    {
        // Do we have a list?
        if (!isset($_POST['list'])) {
            api_security::generateError('400 Bad Request (list)', false);
        }

        // The list is in the form of json to ease the transition
        $list = json_decode($_POST['list']);
        if (json_last_error() != JSON_ERROR_NONE) {
            api_security::generateError('400 Bad Request (json)', false);
        }

        // First get all devices linked to this container
        $devices = [];
        $device = null;
        $stmt = api_database::prepare('SELECT device_uuid FROM storage_device_containers WHERE container_uuid = ?');
        $stmt->bind_param('s', $_POST['container']);
        $stmt->execute();
        $stmt->bind_result($device);
        while ($stmt->fetch()) {
            $devices[] = $device;
        }
        $stmt->close();

        $app = $_SERVER['HTTP_X_APPLICATION_KEY'];

        // Set the new notifications for the devices
        foreach ($list as $item) {
            $data = [
                'identifier' => '',
                'device' => '',
                'title' => $item->title,
                'body' => $item->body,
                'badge' => $item->badge,
                'sound' => $item->sound,
                'time' => (int) $item->time
            ];

            foreach ($devices as $device) {
                $data['identifier'] = $_POST['container'];
                $data['device'] = $device;

                api_request::send($app, APP_KEYS[$app]['secret'], NOTIFICATION_API . '/set', $data);
            }
        }
    }

    // Remove all planned notifications for the container 
    public static function delete(): void
    {
        $app = $_SERVER['HTTP_X_APPLICATION_KEY'];
        api_request::send($app, APP_KEYS[$app]['secret'], NOTIFICATION_API . '/clear', ['identifier' => $_POST['container']]);
    }

    // If we get a new or updated file, we want to let the other linked devices know
    public static function update(string $filename, string $category = 'add-file'): void
    {
        //Empty string?
        if (strlen(trim($filename)) == 0) {
            return;
        }

        // First get all devices linked to this container, accept the current device
        $devices = [];
        $stmt = api_database::prepare('SELECT device_uuid FROM storage_device_containers WHERE device_uuid = ? AND container_uuid = ?');
        $stmt->bind_param('ss', $_POST['device'], $_POST['container']);
        $stmt->execute();
        $stmt->bind_result($device);
        while ($stmt->fetch()) {
            $devices[] = $device;
        }
        $stmt->close();

        $app = $_SERVER['HTTP_X_APPLICATION_KEY'];

        // Send the notifications
        foreach ($devices as $device) {
            if (isset($_POST['device']) && $device == $_POST['device']) {
                continue;
            }

            $data = [
                'device' => $device,
                'content-available' => true,
                'category' => $category,
                'data' => json_encode([
                    'container' => $_POST['container'],
                    'file' => $filename
                ])
            ];

            api_request::send($app, APP_KEYS[$app]['secret'], NOTIFICATION_API . '/send', $data);
        }
    }
}
