<?php

// Quick and dirty, and it works...
include(__DIR__ . '/configuration.php');
include(__DIR__ . '/api-generics/api_database.php');
include(__DIR__ . '/api-generics/api_request.php');
include(__DIR__ . '/api-generics/api_security.php');
include(__DIR__ . '/api-generics/api_uuidv4.php');
include(__DIR__ . '/api_containers.php');
include(__DIR__ . '/api_files.php');
include(__DIR__ . '/api_notifications.php');
include(__DIR__ . '/api_clean.php');

// Handle cli requests (cron jobs)
if (php_sapi_name() == 'cli') {
    if (!isset($argv[1])) {
        exit(0);
    }

    switch ($argv[1]) {
        case 'clean':
            api_clean::deletedfiles();
            break;
    }

    exit(0);
}

// Checks if the request is valid, signed and so on
api_security::checkRequest();

// Check if the request contains a container, and record device/container/lastdate
api_containers::is();

// Routing; Just make sure all traffic is rewrited to this file
$action = pathinfo($_SERVER['REQUEST_URI'], PATHINFO_BASENAME);
switch ($action) {
    case 'addcontainer':
        api_containers::add();
        break;
    case 'deletecontainer':
        api_containers::delete();
        break;
    case 'listfiles':
        api_files::list();
        break;
    case 'getfile':
        api_files::get();
        break;
    case 'addfile':
        api_files::add();
        break;
    case 'deletefile':
        api_files::delete();
        break;
    case 'setnotifications':
        api_notifications::set();
        break;
    case 'delnotifications':
        api_notifications::delete();
        break;
    default:
        self::generateError('404 File not found', false);
}

// If everything is fine, we do not return any data, just a 200 status code, exept when it is expected
