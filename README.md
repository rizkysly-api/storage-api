# Storage API 
Store files on the server to share between devices. Files are stored in containers. On changes, the devices receive a notification.

## Installation
`git clone https://gitlab.com/rizkysly-api/storage-api.git`  
`git submodule update --init --recursive`

Import the `database.sql` file into your mysql database.

Rename `configuration_example.php` to configuration.php and change the files constants.  

Setup an url-rewrite rule to send all traffic to `index.php`.  

## Requests
Please see https://gitlab.com/rizkysly-api/api-generics.git for request specifications. The post values here are on top of the once described in api-generics.

Make sure the client handles the following response.  
>>>
`204 No content`  
The container has been deleted and the client should delete it as well.  
>>>

**/addcontainer**  
Adds a container to the server.  
>>>
container=uuidv4
>>>

**/deletecontainer**  
Deletes the containers content from the server and marks it deleted for future requests.  
>>>
container=uuidv4
>>>

**/listfiles**  
Returns a list of all files with md5 checksums within the container.
>>>
container=uuidv4
>>>

**/getfile**  
Download a specific file from the server.
>>>
container=uuidv4  
file=filename
>>>

**/addfile**  
Add a file to the server.  
>>>
container=uuidv4  
file=filename  
data=file data in base64 format
>>>

**/deletefile**  
Delete a specific file from the server.
>>>
container=uuidv4vv  
file=filename
>>>

**/setnotifications**  
Plans notifications for all devices that use the same container.
>>>
container=uuidv4vv  
list=[{"title":"","body":"","badge":1,"time":0}]
>>>

**/delnotifications**  
Deletes notifications for all devices that use the same container.
>>>
container=uuidv4
>>>
